import maya.OpenMaya as om
import maya.OpenMayaAnim as oma
import maya.cmds as mc
import maya.mel as mm


def unlock_trs(node_name):
    for attr in "trs":
        for ax in "xyz":
            mc.setAttr("{}.{}{}".format(node_name, attr, ax), l=False)


def dup_and_clean_unused_intermediate_shapes(node_name):
    dupped_name = mc.duplicate(node_name, rr=True)[0]
    unlock_trs(dupped_name)
    shapes = mc.listRelatives(dupped_name, s=True, f=True)
    if shapes:
        for shape in shapes:
            if mc.getAttr("{}.intermediateObject".format(shape)):
                mc.delete(shape)
    if mc.listRelatives(dupped_name, p=True):
        mc.parent(dupped_name, w=True)

    return dupped_name


def main(geo_name):
    skinname = mm.eval('findRelatedSkinCluster "{}"'.format(geo_name))

    sellist = om.MSelectionList()
    sellist.add(geo_name)
    sellist.add(skinname)

    dag = om.MDagPath()
    sellist.getDagPath(0, dag)

    skinobj = om.MObject()
    sellist.getDependNode(1, skinobj)
    skinfn = oma.MFnSkinCluster(skinobj)

    infarray = om.MDagPathArray()
    skinfn.influenceObjects(infarray)
    inf_count_util = om.MScriptUtil(infarray.length())
    inf_count_ptr = inf_count_util.asUintPtr()

    infnames = [infarray[ix].fullPathName() for ix in range(infarray.length())]

    if infarray.length() == 1:
        dupped_dag = dup_and_clean_unused_intermediate_shapes(geo_name)
        unlock_trs(dupped_dag)
        mc.parentConstraint(infnames[0], dupped_dag, mo=True)
        mc.scaleConstraint(infnames[0], dupped_dag, mo=True)

        return dupped_dag

    weight_array = om.MDoubleArray()
    empty_object = om.MObject()
    skinfn.getWeights(dag, empty_object, weight_array, inf_count_ptr)

    weights = [
        list(weight_array[i : i + infarray.length()])
        for i in range(0, len(weight_array), infarray.length())
    ]

    face_grps = [[] for ix in range(infarray.length())]
    mit = om.MItMeshPolygon(dag)
    while not mit.isDone():
        vtcs = om.MIntArray()
        mit.getVertices(vtcs)
        sum_ = None
        for vtx in vtcs:
            skinvals = weights[vtx]
            if sum_:
                sum_ = [oval + val for oval, val in zip(sum_, skinvals)]
            else:
                sum_ = skinvals
        face_grps[sum_.index(max(sum_))].append(mit.index())
        mit.next()

    proxies = []
    for ix, face_grp in enumerate(face_grps):
        if not len(face_grp):
            continue
        proxy = mc.duplicate(geo_name, rr=True)[0]
        unlock_trs(proxy)
        if mc.listRelatives(proxy, p=True):
            mc.parent(proxy, w=True)

        faces = [
            "{}.f[{}]".format(proxy, f)
            for f in range(mc.polyEvaluate(proxy, f=True))
            if not f in face_grp
        ]
        mc.delete(faces)

        mc.parentConstraint(infnames[ix], proxy, mo=True)
        mc.scaleConstraint(infnames[ix], proxy, mo=True)

        proxies.append(proxy)

    return proxies
